﻿namespace AspNetCoreWithReact.WebUI.Models
{
    public class AppSettings
    {
        public bool UseUseProxyToSpaDevelopmentServer { get; set; }
        public string ClientAppServerUrl { get; set; }
    }
}

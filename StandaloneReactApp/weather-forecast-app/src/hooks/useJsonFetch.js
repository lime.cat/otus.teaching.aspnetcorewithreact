import { useState, useEffect } from 'react';

export default function useJsonFetch(url, opts) {
    let [data, setData] = useState(null);
    let [loading, setLoading] = useState(false);
    let [error, setError] = useState(null);

    useEffect(() => {
        setLoading(true);
        async function fetchData() {
            try {
                const response = await fetch(url, opts);
                if (response.ok) 
                    setData(await response.json());
                else 
                    setError(await response.json());
            }
            catch (err) {
                setError(err);
            }
            finally {
                setLoading(false);
            }
        } 
        fetchData();
    }, [url, opts]);    
    
    return [data, loading, error];
}
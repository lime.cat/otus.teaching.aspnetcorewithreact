import useJsonFetch from '../hooks/useJsonFetch';
import env from 'react-dotenv';

function Forecast() {
    const [data, loading, error] = useJsonFetch(env.FORECAST_API_URL);
    
    const dataComponent = !loading && data && 
        (<table>
            <thead>
                <tr>
                    <th>Дата</th>
                    <th>Температура, C</th>
                    <th>Описание</th>
                </tr>
            </thead>
            <tbody>
                {data.map(x =>
                    <tr key={x.date}><td>{formatDate(new Date(x.date))}</td><td>{x.temperatureC}</td><td>{x.summary}</td></tr>
                )}
            </tbody>
        </table>);

    return (<div className="container">
        <h2 className="table-header">Прогноз</h2>
            {loading && <div>Загрузка...</div>}
            {dataComponent}
            {!loading && error && <div>Возникла ошибка получения данных</div>}
    </div>);
  }
  
  function formatDate(date) {
    return `${date.getDate()}.${date.getMonth() + 1}.${date.getFullYear()}`;
  }

  export default Forecast